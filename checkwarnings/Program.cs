﻿namespace CheckWarnings
{
    using System;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;
    using CommandLine;
    
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Report
    {
        [XmlArrayItem("Project", IsNullable = false)]
        public ReportProject[] Projects { get; set; }
    }
    
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType = true)]
    public class ReportProject
    {
        [XmlElement("Issue")]
        public ReportProjectIssue[] Issues { get; set; }
    }
    
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType = true)]
    public  class ReportProjectIssue
    {
    }

    internal static class Program
    {
        // ReSharper disable once ClassNeverInstantiated.Local
        private class Options
        {
            [Option('f', "file", Required = true, HelpText = "file to check")]
            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public string File { get; set; }
            [Option('t', "threshold", Required = true, HelpText = "Maximum number of warnings allowed")]
            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public int Threshold { get; set; }
        }

        private static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
                .WithParsed(RunOptions);

        }
        
        private static void RunOptions(Options options)
        {
            try
            {
                var report = new XmlSerializer(typeof(Report)).Deserialize(new XmlTextReader(options.File)) as Report;
                var issueCount = report?.Projects?.Where(project => report.Projects != null)
                    .Sum(project => project.Issues.Length) ?? 0;
                Console.WriteLine($"Found {issueCount} Warnings is { (issueCount > options.Threshold ? "over" : "under")} threshold");
                Environment.Exit(issueCount > options.Threshold ? 1 : 0);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(1);
            }
        }

    }
}
