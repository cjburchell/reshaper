FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY . .

WORKDIR /src/checkwarnings
RUN dotnet restore "checkwarnings.csproj" \
    && dotnet build "checkwarnings.csproj" -c Release -o /app/build \
    && dotnet publish "checkwarnings.csproj" -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/sdk:7.0

RUN wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb -q \
  && dpkg -i packages-microsoft-prod.deb \
  && apt-get update && apt-get install -y --no-install-recommends dotnet-sdk-3.1=3.1.405-1 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && dotnet tool install JetBrains.ReSharper.GlobalTools --version 2021.2.2 --tool-path /tools
  
COPY --from=build /app/publish /tools/checkwarnings
